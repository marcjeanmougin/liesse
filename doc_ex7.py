def add(x, y):
    """ Ajoute deux nombres.
    Exemple :
    >>> add(1, 3)
    4
    """
    return x + y

