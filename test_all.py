import math
import pytest
import random

import doc_ex1
import doc_ex2
import doc_ex14

def test_pgcd():
    for i in range(20):
        a = random.randint(1,100)
        b = random.randint(1,100)
    lcm = a * b / math.gcd(a, b)
    assert(doc_ex1.f(a, b) == math.gcd(a, b))
    assert(doc_ex1.g(a, b) == lcm)
    assert(doc_ex2.pgcd(a, b) == math.gcd(a, b))
    assert(doc_ex2.ppcm(a, b) == lcm)
    assert(doc_ex14.euclide(a, b) == math.gcd(a, b))
    (d, u, v) = doc_ex14.euclide2(a,b)
    assert( a*u + b*v == d)
    assert( d == math.gcd(a, b))

import doc_ex4
import doc_ex9
import doc_ex15

def test_fibo():
    assert(doc_ex4.f(42) == 267914296)
    assert(doc_ex4.f(142) == 212207101440105399533740733471)
    assert(doc_ex9.f(42) == 267914296)
    assert(doc_ex15.fibo(10) == 55)
    assert(doc_ex15.fibo2(42) == 267914296)

@pytest.mark.xfail
def test_fibo9_2():
    assert(doc_ex9.f(142) == 212207101440105399533740733471)

import doc_ex8

def test_remove_last():
    l = [1,2,3,4,3]
    doc_ex8.remove_last(l,3)
    assert(l == [1,2,3,4])

# programme volontairement bugué
@pytest.mark.xfail
def test_remove_last_2():
    l = [1,2,3,4,3]
    doc_ex8.remove_last(l,5)
    assert(l == [1,2,3,4,3])

import doc_ex10

def test_add_all():
    l = [1,2,3,4,3]
    assert(doc_ex10.add_all(l) == 13)

import doc_ex11

def test_tri_fusion():
    l = [1,2,3,4,3]
    assert(doc_ex11.tri_fusion(l) == [1,2,3,3,4])

import doc_ex12

def test_collatz():
    for i in range(20):
        a = random.randint(1,100)
        assert(doc_ex12.collatz(a))

import doc_ex13

def test_fact():
    assert(doc_ex13.fact(42) == 1405006117752879898543142606244511569936384000000000)



