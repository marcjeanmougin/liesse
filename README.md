# Stage Liesse : Méthodes de programmation illustrées en Python

## Marc Jeanmougin, 29 Avril 2021


 [![pipeline status](https://gitlab.com/marcjeanmougin/liesse/badges/master/pipeline.svg)](https://gitlab.com/marcjeanmougin/liesse/-/commits/master) 
 [![coverage report](https://gitlab.com/marcjeanmougin/liesse/badges/master/coverage.svg)](https://gitlab.com/marcjeanmougin/liesse/-/commits/master) 

Ce repository contient les sources du PDF et les divers codes Python présentés lors du stage.

L'intégration continue de ce projet permet de compiler automatiquement le PDF, qui est servi sur https://marcjeanmougin.gitlab.io/liesse/ .










