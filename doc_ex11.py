def tri_fusion(L):
    if len(L) <= 1:
        return L
    L1 = tri_fusion(L[0:len(L)//2])
    L2 = tri_fusion(L[len(L)//2:len(L)])
    return fusion(L1, L2)
def fusion(L1, L2):
    if(L1==[]): return L2
    if(L2==[]): return L1
    if(L1[0] < L2[0]):
        return [L1[0]]+fusion(L1[1:], L2)
    else:
        return [L2[0]]+fusion(L2[1:], L1)

