def fibo(n):
    if n == 0:
        return 0
    if n == 1:
        return 1
    return fibo(n-1) + fibo(n-2)

f = {0:0, 1:1}
def fibo2(n):
    if n in f:
        return f[n]
    f[n] = fibo2(n-1)+fibo2(n-2)
    return f[n]
