def euclide(a, b):
    while(b):
      (a, b) = (b, a%b)
    return a

def euclide2(a, b):
    if b==0:
        return (a,1,0)
    (d, u, v) = euclide2(b, a % b)
    return (d, v, u-v*(a//b)) 

