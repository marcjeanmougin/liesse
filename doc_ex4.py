def f(n):
    if n < 0:
        raise ValueError
    return g(n)[0]

def g(n):
    if n == 0:
        return (0, 1)
    a, b = g(n // 2)
    c = a * (b * 2 - a)
    d = a * a + b * b
    return (d, c + d) if n % 2 else (c, d)
