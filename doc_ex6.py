import pytest

def add(x, y):
    return x + y

@pytest.mark.xfail
def test_add():
    assert add(0.1, 0.2) == 0.3
