def remove_last(liste, element):
    pos = -1;
    for i in range(len(liste)):
        if liste[i] == element:
            pos = i
    del liste[pos]

def test_remove_list():
    l = [1,2,3,3,4]
    remove_last(l,3)
    assert(l == [1,2,3,4])

